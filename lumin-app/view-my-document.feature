Feature: My document
  As a user
  I want to view my document
  So that I can work with it.

  Background:
    Given I am on the app page "/"
    And I logged in

  Scenario Outline: Viewing my document
    When I click on button "#login-dropdown-list .icon-user"
    Then I see the dropdown menu '.dropdown-menu' is open
    When I click on button "#drop-my-documents"
    Then The app is still this pages
    And  I see my list documents, they are filtered by time
    When I click on one document <file_name>
    Then The app show some menu icons
    And I select one <icon> to do <action> with that document

 Examples:
    | file_name     | icon               | action                   |
    | image.jpg     | "#t2DeleteDoc"     | Delete                   |
    | document.pdf  | "#t2OpenDoc"       | Open document            |
    | slide.ppt     | "#t2OpenDocNewTab" | Open document on new tab |

 Scenario Outline: Switching view layout
    When I am viewing my documents with <current_layout>
    When I click button ".pick-info"
    Then The app show my documents with <new_layout>

 Examples:
    | current_layout | new_layout |
    | list           | thumbnail  |
    | thumbnail      | list       |
