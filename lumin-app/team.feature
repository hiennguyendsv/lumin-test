Feature: View and managing team list
  As a user
  With many fellows
  I want to buy premium plan for them, all at once
  So I create a new team.

  Background:
    Given I am a user of LuminPDF
    And I have logged in
    And I am on the 'Team' page

  Scenario: Create a new team
    When I click on 'Create Team' button
    Then I should be directed to 'Create team' page
    When I have entered my Team name
    And I have picked my Team color
    And I click on 'Create Team' button
    Then I should be redirected back to 'Team' page
    And my new team should be listed in the team list

  Group:
  @Action-dropdown-menu-team-page
    Background:
      Given I am admin of an existing team
      When I click on the button on the 'Action' column of said team
      Then I should see an Action dropdown menu

    Scenario: Edit a team settings
      When I select the 'Edit Settings' option on the dropdown menu
      Then I should be directed to 'Edit team' page of my team
      When I have made my changes to team's name and color
      And I click on 'Save' button
      Then I should be redirected back to 'Team' page
      And my changes on my team settings should be saved

    Scenario: Go to Team detail page to add new member
      When I select the 'Invite Member' option on the dropdown menu
      Then I should be directed to my Team detail page

    Scenario: Delete a team
      When I select the 'Remove' option on the dropdown menu
      Then I should see a pop-up alert asking me to confirm if I really want to remove that team
      When I click on the 'Remove' button on the alert
      Then the alert should disappear
      And the team I want to delete should disappear from the team list as well

  Group:
  @Team-detail-page
    Background:
      Given I am admin of an existing team
      And I am on the detail page of said team

    Scenario Outline: Add a new team member
      Given there is a user with <email> and <username>
      When I filled in <email> at the 'Search by email address' field
      Then I should see a dropdown list with <username> listed
      When I select <username> on the list
      Then I should see a <message>
      And their account should appear on my team member list with <username>, <email> and <role>
      When user log in with <email> and <password>
      Then they should see the team on their Team page

    Examples:
      | username | password   | email           | role   | message |
      | user1    | dsv123!@#  | user1@gmail.com | member | user1 has been successfully invited to your team |
      | user2    | dsv123!@#  | user2@gmail.com | member | user2 has been successfully invited to your team |

    Scenario: Add an invalid user
      When I filled in an invalid email address "email@abc"
      Then I should see a message "No members found"

    Group:
    @Action-dropdown-menu-detail-page
      Background:
        Given my team has at least one member
        When I click on the button on the 'Action' column of a team member
        Then I should see an Action dropdown menu

      Scenario: Remove a team member
        When I select the 'Remove' option on the dropdown menu
        Then I should see a pop-up alert asking me to confirm if I really want to remove that team member
        When I click on the 'Remove' button on the alert
        Then the alert should disappear
        And the team member I want to remove should disappear from the team member list as well
        When the user log in
        Then they should no longer see the team on their Team page

      Scenario: Make a team member Admin
        When I select the 'Make Admin' option on the dropdown menu
        Then I should see a pop-up alert asking me to confirm if I really want to make that team member as Admin
        When I click on the 'OK' button on the alert
        Then the alert should disappear
        And the team member I want to make as Admin should have their role changed to "Admin"
        And my role should be changed to "Member"
        And I can't see the Add member input and Action button anymore
        When the new Admin log in
        Then they should see their role has changed to "Admin"
        And they can now manage the team
