Feature: Select menu to view guide page and contact page
  As a user
  I want to select menu
  So that I can view pages in the app.

  Background:
    Given I am on the app page "/"

  Scenario Outline: Click to the menu to view page
    When I click on button <menu>
    Then The app open a new tab for the <page>

  Examples:
    | menu                                           | page                        |
    | "#navbar a[href='https://guide.luminpdf.com']" | https://guide.luminpdf.com/ |
    | "#navbar a[href='/static/contact']"            | /static/contact             |

  Scenario: Send a message to developers
    Given I am on the page "/static/contact"
    Then I enter my name to"#send-message input[name='name']"
    And I enter my email to  "#send-message input[name='email']"
    And I enter message to  "#comment"
    And I click to ".recaptcha-checkbox-checkmark"
    And I click to ".submit"
    Then The app show the message: "Thank you for your feedback!"
