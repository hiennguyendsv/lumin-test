Feature: Upload File
  As a user
  I want to upload file
  So that I can use Lumin App to edit it.

  Background:
    Given I am on the app page "https://app.luminpdf.com/"
    And I logged in

  Scenario Outline: Upload file from local
    When I click on button "#file-upload"
    And I choose file <file_name> from my computer
    Then I should be redirected to viewer page to edit my file

  Examples:
    | file_name     |
    | image.jpg     |
    | document.docx |
    | document.pdf  |

  Scenario Outline: Upload file from cloud service
    Given I want to upload file from <cloud_service>
    When I click on button <upload_button>
    And I allow Lumin App to manage my <cloud_service>
    And I choose file <file_name> from my <cloud_service>
    Then I should be redirected to viewer page to edit my file

  Examples:
    | cloud_service | upload_button          | file_name     |
    | Google Drive  | .icon-icon_googledrive | image.jpg     |
    | Google Drive  | .icon-icon_googledrive | image.png     |
    | Drop Box      | .icon-dropbox          | document.docx |
    | Drop Box      | .icon-dropbox          | document.doc  |
    | Box           | .icon-box              | document.pdf  |
    | Box           | .icon-box              | document.xlsx |
    | Box           | .icon-box              | document.xls  |
    | OneDrive      | .icon-clouddrive       | document.ppt  |
    | OneDrive      | .icon-clouddrive       | document.pptx |

Scenario Outline: Upload file that app doesn't support
  Given I want to upload file has <extension> from local
  When I click on button "#file-upload"
  And I choose file <file_name> from my computer
  Then The App show a message "Error! Lumin can not open this file"

Examples:
  | extension | file_name |
  | .rar      | file.rar  |
  | .zip      | file.zip  |
  | .md       | readme.md |
  | .txt      | note.txt  |
