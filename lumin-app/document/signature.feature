Feature: Creating signature for my document
  As a user
  I want to create signature with my document

  Background:
    Given I am viewing one document
    And  I logged in

  Scenario: Creating signature for my document
    Given My account is not Premium
    When I click on button "#signatureBtnWrapper"
    Then The app show a modal "#notice-premium" with message "This feature requires Lumin Premium"
    When I click on "#close-modal-notice"
    Then The modal is closed
    And I can't create signature for my document
    When I click on button "#go-premium"
    Then The app open the page "/pricing" on a new tab
    When I click on button ".btn-submit-price .btn-premium"
    Then I should be redirected to page "/payments"
    When I enter the information in  "#card-holder", "#card-number", "#card-exp-date", "#card-ccv"
    And I check to "#check-terms"
    And I click to "#submit-payment"
    Then ????

  
