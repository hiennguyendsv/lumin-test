Feature: Working with my document
  As a user
  I want to working with my document

  Background:
    Given I am viewing one document
    And  I logged in

  Scenario: Downloading my document with annotations
    When I click on button "#icon-download2"
    Then The app show a overlay "#downloadPDFOverLay"
    When I choose type of output file is PDf
    And I click on "#radiobtnDownloadWithAnnotations"
    And I click on button "#btnStartDownloadFile"
    Then The app start download my document with annotations
    And I can use it local from my computer

  Scenario: Downloading my document without annotations
    When I click on button "#icon-download2"
    Then The app show a overlay "#downloadPDFOverLay"
    When I choose type of output file is PDf
    And I click on "#radiobtnDownloadWithoutAnnotations"
    And I click on button "#btnStartDownloadFile"
    Then The app start download my document without annotations
    And I can use it local from my computer

  Scenario: Printing my document with annotations
    When I click on button "#icon-print2"
    Then The app show a overlay "#PrintPDFOverLay"
    When I click on "#printWithAnnotations"
    Then The print previewer is opened
