Feature: Undo and redo document
  As a user
  I want to undo or redo my document

  Background:
    Given I am viewing one document
    And  I logged in

  Scenario: Undo and redo document
    Given I added text box on my document
    When  I click on "#secondaryToolbarToggle"
    Then  The Toolbar will be appeared
    When  I click to "#viewUndo"
    Then  The text box will be disappeared
    When  I click to "#viewRedo"
    Then  The text box will be appeared again
