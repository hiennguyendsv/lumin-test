Feature: Rectangle drawing
  As a user
  I want to draw a rectangle on my document

  Background:
    Given I am viewing one document
    And  I logged in

  Scenario: Rectangle drawing
    Given I chose color "#9043e7"
    When  I click on "#whiteout"
    And   I drag from top left corner to bottom right corner
    Then  The rectangle with color "#9043e7" is appeared on my document

  Scenario: Editing rectangle
    Given I have a rectangle with color "#9043e7"
    When  I click on the rectangle
    Then  Annotation Toolbar will be appeared
    When  I click on ".annotation-pickcolor"
    Then  The app show color palette ".list-pickers-color"
    When  I choose color "#23ef0e"
    Then  That rectangle has color "#23ef0e"

  Scenario: Deleting rectangle
    Given I have a rectangle with color "#23ef0e"
    And   Annotation Toolbar appear
    When  I click to (".delete")
    Then  Annotation rectangle will be deleted
