Feature: Strikethrough text in my document
  As a user
  I want to strikethrough text in my document

  Background:
    Given I am viewing one document
    And  I logged in

  Scenario: Strikethrough text
    Given I chose color "#f4cb0d"
    When  I click on "#strikethrough"
    And   I hightlight the text that I want to do strikethrough
    Then  The text is strikethroughed with color "#f4cb0d"

  Scenario: Editing strikethroughed text
    Given I have a strikethroughed text with color "#ed1145"
    When  I click on the strikethroughed text
    Then  Annotation Toolbar will be appeared
    When  I click on ".annotation-pickcolor"
    Then  The app show color palette ".list-pickers-color"
    When  I choose color "#23ef0e"
    Then  The text will be strikethroughed with color "#23ef0e"
    When  I click to "#popover-line-width"
    And   I choose line size
    Then  The line has that size

  Scenario: Deleting strikethroughed text
    Given I have a strikethroughed text with color "#23ef0e"
    And   Annotation Toolbar appear
    When  I click to (".delete")
    Then  Annotation strikethrough will be delete
