Feature: Highlight text in my document
  As a user
  I want to highlight text in my document

  Background:
    Given I am viewing one document
    And  I logged in

  Scenario: Highlight text
    Given I chose color "#f4cb0d"
    When  I click on "#highlightBtn"
    And   I hightlight the text that I want to do hightlight
    Then  The text is hightlighted with color "#f4cb0d"

  Scenario: Editing highlighted text
    Given I have a hightlighted text with color "#ed1145"
    When  I click on the hightlighted text
    Then  Annotation Toolbar will be appeared
    When  I click on ".annotation-pickcolor"
    Then  The app show color palette ".list-pickers-color"
    When  I choose color "#23ef0e"
    Then  The text will be hightlighted with color "#23ef0e"

  Scenario: Deleting highlighted text
    Given I have a hightlighted text with color "#23ef0e"
    And   Annotation Toolbar appear
    When  I click to (".delete")
    Then  Annotation highlight will be deleted
