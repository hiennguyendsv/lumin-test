Feature: Page navigation
  As a user
  I want to navigate pages in my document

  Background:
    Given I am viewing one document
    And  I logged in

  Scenario: Page navigation
    When I click on "#next"
    Then The app show the next page
    When I click "#previous"
    Then The app show the previous page

  Scenario: First page
    When I click on "#secondaryToolbarToggle"
    Then The Toolbar will be appeared
    When I click on "#firstPage"
    Then The app show the first page

  Scenario: Last page
    When I click on "#secondaryToolbarToggle"
    Then The Toolbar will be appeared
    When I click on "#lastPage"
    Then The app show the last page
