Feature: Adding text box to my document
  As a user
  I want to add text box to my document

  Background:
    Given I am viewing one document
    And  I logged in

  Scenario: Adding text box to my document
    Given I chose color "#9043e7"
    When  I click on button "#textbox"
    And   I click anywhere on my document that I want to add text box
    Then  The text box is created
    And   I can edit content of text box

  Scenario: Editing text box
    Given I have a text box with color "#9043e7"
    When  I click on the free line
    Then  Annotation Toolbar will be appeared
    When  I drag button ".move" to move text box
    Then  The text box will be move to where I drop text box
    When  I click on ".annotation-pickcolor"
    Then  The app show color palette ".list-pickers-color"
    When  I choose color "#ed1145"
    Then  The free line will be changed color to "#23ef0e"
    When  I click to "#text-size"
    And   I choose text size
    Then  The text has that size
    When  I click ".fa-bold"
    Then  The text is made bold
    When  I click ".fa-italic"
    Then  The text is italicized
    When  I click ".fa-underline"
    Then  The text is underlined

  Scenario: Deleting text box
    Given I have a text box with color "#23ef0e"
    And   Annotation Toolbar appear
    When  I click to (".delete")
    Then  Text box will be deleted
