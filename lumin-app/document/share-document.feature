Feature: Sharing document
  As a user
  I want to share my document

  Background:
    Given I am viewing one document
    And  I logged in

  Scenario: Sharing document
    When I click on "#secondaryToolbarToggle"
    Then The Toolbar will be appeared
    When I click on "#share"
    Then The app show modal sharing
    When I click on "#copyShareLink"
    Then I can send link document to person that want to share
    When I enter email addresses that I want to share
    And I click on "#read-permission"
    Then The reciever has read permission with that document
    And I click on "#write-permission"
    Then The reciever has write permission with that document
    When I click on "#btn-share-document"
    Then The modal will close
