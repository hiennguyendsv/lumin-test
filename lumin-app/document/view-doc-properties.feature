Feature: Viewing document properties
  As a user
  I want to view document properties

  Background:
    Given I am viewing one document
    And  I logged in

  Scenario: Viewing document properties
    When  I click on "#documentProperties"
    Then  The app show the modal
    And   I can see properties of document
    When  I click on "#documentPropertiesClose"
    Then  The modal will close
