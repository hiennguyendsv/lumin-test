Feature: Underline text in my document
  As a user
  I want to underline text in my document

  Background:
    Given I am viewing one document
    And   I logged in

  Scenario: Underline text in my document
    Given I chose color "#ed1145"
    When  I click on "#underline"
    And   I hightlight the text that I want to underline
    Then  The text is underlined with "#ed1145"

  Scenario: Editing underlined text
    Given I have a underlined text with color "#9043e7"
    When  I click on the underlined text
    Then  Annotation Toolbar will be appeared
    When  I click on ".annotation-pickcolor"
    Then  The app show color palette ".list-pickers-color"
    When  I choose color "#ed1145"
    Then  The free line will be changed color to "#23ef0e"
    When  I click to "#popover-line-width"
    And   I choose line size
    Then  The line has that size

  Scenario: Deleting underlined text
    Given I have a underlined text with color "#23ef0e"
    And   Annotation Toolbar appear
    When  I click to (".delete")
    Then  Text box will be deleted
