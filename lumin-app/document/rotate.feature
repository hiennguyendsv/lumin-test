Feature: Rotating document
  As a user
  I want to rotate my document

  Background:
    Given I am viewing one document
    And  I logged in

  Scenario: Rotating document
    Given The orientation of the document is portrait
    When I click to "#pageRotateCw"
    Then The orientation of the document is landscape
    When I click to "#pageRotateCw"
    Then The orientation of the document is reverse portrait
    When I click to "#pageRotateCw"
    Then The orientation of the document is reverse landscape
