Feature: Choose color to edit my document
  As a user
  I want to choose one color
  So that I can edit my document with that color

  Background:
    Given I am viewing one document
    And   I logged in

  Scenario: Choose color
    When I click "#editor-color" to choose color
    Then The app show color palette ".list-pickers-color"
    When I choose color "#ed1145"
    Then I can edit my document with color "#ed1145"
