Feature: Free line drawing
  As a user
  I want to draw free line on my document

  Background:
    Given I am viewing one document
    And  I logged in

  Scenario: Free line drawing
    Given I chose color "#9043e7"
    When  I click on "#freeline"
    And   I draw anything on my document with color that I chose
    Then  The free lines that I drew appear on my document

  Scenario: Editing free line
    Given I have a free line with color "#9043e7"
    When  I click on the free line
    Then  Annotation Toolbar will be appeared
    When  I click on ".annotation-pickcolor"
    Then  The app show color palette ".list-pickers-color"
    When  I choose color "#ed1145"
    Then  The free line will be changed color to "#23ef0e"
    When I click to "#popover-line-width"
    And I choose line size
    Then The free line has that size

  Scenario: Deleting free line
    Given I have a free line with color "#23ef0e"
    And   Annotation Toolbar appear
    When  I click to (".delete")
    Then  Free line will be deleted
