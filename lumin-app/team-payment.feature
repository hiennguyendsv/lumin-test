Feature: Team Payment
  As a team member
  I want to buy Premium plan for my team

  Background:
    Given I am a user
    And I am a member of a team
    And I have logged in
    And I am on the detail page of my team

  Scenario Outline: Team Premium Payment
    When I click on the "Go Premium" button
    Then I should be directed to the page with title 'Lumin Premium'
    And I should see my team's name is selected on the 'Team Name' field
    And I should see the bill information displays my team number of member correctly
    And the total money should be equal to price per user multiple by number of member
    When I fill in my card's information
    And I tick the "I accept all terms and conditions"
    And I click the 'Complete Purchase' button
    Then I should be directed to 'Subscription success' page
    And I should see the <announcement>
    And I should see the announcement about the renew date and total money
    And I should see a 'Cancel Subscription' button
    And I should see my invoice history for the team
    When I back to the Team page
    Then I should see my team status changed to "Premium"

  Examples:
    | team_name | number_of_member | plan     | announcement |
    | team1     | 3                | monthly  | Your team team1 is currently subscribed to Lumin Premium |
    | team2     | 1                | yearly   | Your team team2 is currently subscribed to Lumin Premium |

  Scenario: Cancel team subscription
    When I click on the 'Cancel Subscription' button
    Then I should see a pop-up alert asking me to confirm if I really want to cancel the subscription
    When I click on the 'Remove' button on the alert
    Then the alert should disappear
    And the 'Cancel Subscription' button should change to 'Continue Subscription' button

  Scenario: Continue team subscription
    Given I have purchased a premium plan for my team
    When the plan renew date is over
    Then I should see a 'Continue Subscription' button in place of the 'Cancel Subscription' button
    When I click on the 'Continue Subscription' button
    Then I should be directed to the page with title 'Lumin Premium' 
