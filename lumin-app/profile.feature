Feature: Viewing and editting profile
  As a user
  I want to view my profile
  So that I can edit it.

  Background:
    Given I am on the app page "/"
    And I logged in

  Scenario: Viewing my profile
    When I click on button "#login-dropdown-list .icon-user"
    Then I see the dropdown menu '.dropdown-menu' is open
    When I click on "#login-buttons-edit-profile"
    Then I should be redirected to page "/settings" to view and edit my profile

  Scenario Outline: Deleting account
    Given I am on the app page "/settings"
    When I click on button "#delete-my-account"
    Then I see a modal with message “DO YOU REALLY WANT TO DELETE YOUR ACCOUNT?”
    When I click on button "button[data-bb-handler=cancel]"
    Then The app still the page: "/settings"
    When I click on button "#delete-my-account" again
    And  I click on button "button[data-bb-handler=confirm]"
    Then I should be redirected to '/'
    And  I cannot log in again with that account
