Feature: Sign up the tool
  As a user
  I want to sign in the tool
  So that I can use the tool

  Background:
    Given I am on the page "/"

  Scenario: Signing in with my account on the tool
    When I click on button ".menu-primary btn-signin"
    Then I shoule be redirected to page "/login"
    When I enter my email and password
    And  I click on button ".btn-sign"
    Then I shoule be redirected to page "/split-merge"
    And  I can use the tool
