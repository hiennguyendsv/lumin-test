Feature: Signing up the tool
  As a user
  I want to sign up the tool
  So that I can use the tool

  Background:
    Given I am on the page "/"

  Scenario: Signing up account on the tool
    When I click on button ".menu-primary btn-signup"
    Then I shoule be redirected to page "/sign-up"
    When I enter my name, email and password
    And  I click on button ".btn-sign"
    Then I shoule be redirected to page "/split-merge"
    And  I should be signed in to the tool with my account that I signed up
